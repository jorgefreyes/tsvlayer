package com.fulgent.tsvlayer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The actual workhorse of the whole bunch. This reads, parses the file and 
 * populates the POJOs ParsedAnalysisResultFile and ParsedAnalysisResult.
 * @author jspindola
 *
 */
public class ReadAnalysisResultFile {
	
	private ParsedAnalysisResult par;
	private ParsedAnalysisResultFile parFile;
	
	public ReadAnalysisResultFile(String in) {
		this.par = new ParsedAnalysisResult();
		this.parFile = new ParsedAnalysisResultFile();
	}
	
	public ParsedAnalysisResultFile parseAnalysisResultFile(String in) throws IOException {
		File inFile = new File(in);
		FileReader fileReader = new FileReader(inFile);
		int wellNumber = 0;
		BufferedReader buf = null;
		
		try {
			buf = new BufferedReader(fileReader);
			String fetchedLine = null;
			
			while(true) {
				fetchedLine = buf.readLine();
				if (fetchedLine == null) {
					break;
				}
				else {
					String[] tokens = fetchedLine.split("\t");
					if (tokens[0].trim().equalsIgnoreCase("Session Name")) {
						if (tokens.length > 1) {
							parFile.setSessionName(tokens[1].trim());
						}
						continue;
					}
					if (tokens[0].equals("Well")) {
						parFile.setHeaders(tokens);
						continue;
					}
					if (tokens[0].trim().matches("[0-9]+")) {
						wellNumber = Integer.valueOf(tokens[0]);
						//System.out.println("Well number: " + wellNumber);
						par = new ParsedAnalysisResult();
						setFirstRow(tokens);
						continue;
					}
					if (tokens[0].trim().equals("Rn values")) {
						par.setRnValues(setRow(tokens));
						continue;
					}
					if (tokens[0].trim().equals("Delta Rn values")) {
						par.setDeltaRnValues(setRow(tokens));
						continue;
					}
					if (tokens[0].trim().equals("Crt Matrix values")) {
						par.setCrtMatrixValues(setRow(tokens));
					}
					parFile.parsedAnalysisResults.put(wellNumber, par);
				}
			}
		} catch (Exception e) {
			System.out.println("Error detected: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (buf != null) {
				buf.close();
			}
		}
		
		return parFile;
	}
	/**
	 * 
	 * @param data
	 * @return 
	 * 
	 * Sets the individual variables with the values taken from the first row
	 * of each well group. Sometimes not all 16 variables are present so we
	 * introduced the switch statement to deal with the problem.
	 */
	private void setFirstRow(String[] data) {
		int n = data.length;
		switch (n) {
			case 16: par.setCtSE(checkForDbl(data[15].trim()));
			case 15: par.setCqConf(checkForDbl(data[14].trim()));
			case 14: par.setCrtRaw(checkForDbl(data[13].trim()));
			case 13: par.setCrtAmp(checkForDbl(data[12].trim()));
			case 12: par.setAmpScore(checkForDbl(data[11].trim()));
			case 11: par.setStDevQty(checkForDbl(data[10].trim()));
			case 10: par.setAverageQty(checkForDbl(data[9].trim()));
			case 9: par.setQty(checkForInt(data[8].trim()));
			case 8: par.setDeltaCt(checkForDbl(data[7]));
			case 7: par.setCtStDev(checkForDbl(data[6].trim()));
			case 6: par.setAverageCt(checkForDbl(data[5].trim()));
			case 5: par.setCt(checkForDbl(data[4].trim()));
			case 4: par.setTask(data[3].trim());
			case 3: par.setDetector(data[2].trim());
			case 2: par.setSampleName(data[1].trim());
			case 1: par.setWell(Integer.valueOf(data[0].trim()));
		}
	}
	
	private List<Double> setRow(String[] data) {
		List<Double> l = new ArrayList<>();
		for (int i=1; i<data.length; i++) {
			l.add(Double.parseDouble(data[i]));
		}
		return l;
	}
	
	/**
	 * 
	 * @param x
	 * @return Double
	 * 
	 * Converts an empty string into a Double NaN.
	 */
	private Double checkForDbl(String x) {
		if (x.isEmpty()) {
			return Double.NaN;
		}
		return Double.parseDouble(x);
	}
	
	/**
	 * 
	 * @param x
	 * @return Integer
	 * 
	 * Converts an empty string into an Integer value.
	 */
	private Integer checkForInt(String x) {
		if (x.isEmpty()) {
			return 0;
		}
		return Integer.parseInt(x);
	}
}
