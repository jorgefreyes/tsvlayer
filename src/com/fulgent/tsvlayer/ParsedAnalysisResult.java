package com.fulgent.tsvlayer;

import java.util.List;
import java.util.ArrayList;

/**
 * Every object in the ParsedAnalysisResultFile class is an instance of this class.
 * 
 * @author jspindola
 *
 */
public class ParsedAnalysisResult {
	private int well;
	private String sampleName;
	private String detector;
	private String task;
	private double ct;
	private double averageCt;
	private double ctStDev;
	private double deltaCt;
	private int qty;
	private double averageQty;
	private double stDevQty;
	private double ampScore;
	private double crtAmp;
	private double crtRaw;
	private double cqConf;
	private double ctSE;
	List<Double> rnValues;
	List<Double> deltaRnValues;
	List<Double> crtMatrixValues;
	
	public ParsedAnalysisResult() {
		this.rnValues = new ArrayList<>();
		this.deltaRnValues = new ArrayList<>();
		this.crtMatrixValues = new ArrayList<>();
	}

	public int getWell() {
		return well;
	}

	public void setWell(int well) {
		this.well = well;
	}

	public String getSampleName() {
		return sampleName;
	}

	public void setSampleName(String sampleName) {
		this.sampleName = sampleName;
	}

	public String getDetector() {
		return detector;
	}

	public void setDetector(String detector) {
		this.detector = detector;
	}

	public String getTask() {
		return task;
	}

	public void setTask(String task) {
		this.task = task;
	}

	public double getCt() {
		return ct;
	}

	public void setCt(double ct) {
		this.ct = ct;
	}

	public double getAverageCt() {
		return averageCt;
	}

	public void setAverageCt(double averageCt) {
		this.averageCt = averageCt;
	}

	public double getCtStDev() {
		return ctStDev;
	}

	public void setCtStDev(double ctStDev) {
		this.ctStDev = ctStDev;
	}

	public double getDeltaCt() {
		return deltaCt;
	}

	public void setDeltaCt(double deltaCt) {
		this.deltaCt = deltaCt;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public double getAverageQty() {
		return averageQty;
	}

	public void setAverageQty(double averageQty) {
		this.averageQty = averageQty;
	}

	public double getStDevQty() {
		return stDevQty;
	}

	public void setStDevQty(double stDevQty) {
		this.stDevQty = stDevQty;
	}

	public double getAmpScore() {
		return ampScore;
	}

	public void setAmpScore(double ampScore) {
		this.ampScore = ampScore;
	}

	public double getCrtAmp() {
		return crtAmp;
	}

	public void setCrtAmp(double crtAmp) {
		this.crtAmp = crtAmp;
	}

	public double getCrtRaw() {
		return crtRaw;
	}

	public void setCrtRaw(double crtRaw) {
		this.crtRaw = crtRaw;
	}

	public double getCqConf() {
		return cqConf;
	}

	public void setCqConf(double cqConf) {
		this.cqConf = cqConf;
	}

	public double getCtSE() {
		return ctSE;
	}

	public void setCtSE(double ctSE) {
		this.ctSE = ctSE;
	}

	public List<Double> getRnValues() {
		return rnValues;
	}

	public void setRnValues(List<Double> rnValues) {
		this.rnValues = rnValues;
	}

	public List<Double> getDeltaRnValues() {
		return deltaRnValues;
	}

	public void setDeltaRnValues(List<Double> deltaRnValues) {
		this.deltaRnValues = deltaRnValues;
	}

	public List<Double> getCrtMatrixValues() {
		return crtMatrixValues;
	}

	public void setCrtMatrixValues(List<Double> crtMatrixValues) {
		this.crtMatrixValues = crtMatrixValues;
	}
	
	
}
