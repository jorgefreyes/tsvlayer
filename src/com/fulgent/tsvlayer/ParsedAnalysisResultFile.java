package com.fulgent.tsvlayer;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * This is the final product of the analysis_result.txt file parsing.
 * It's a POJO. I expect that Joe Fierro wil determine what to do with it.
 * 
 * @author jspindola
 *
 */
public class ParsedAnalysisResultFile {
	private String sessionName;
	private String[] headers;
	Map<Integer, ParsedAnalysisResult> parsedAnalysisResults;
	
	public ParsedAnalysisResultFile() {
		this.parsedAnalysisResults = new LinkedHashMap<>();
	}

	public String getSessionName() {
		return sessionName;
	}

	public void setSessionName(String sessionName) {
		this.sessionName = sessionName;
	}

	public String[] getHeaders() {
		return headers;
	}

	public void setHeaders(String[] headers) {
		this.headers = headers;
	}
	
	public ParsedAnalysisResult getWellData(int i) {
		return parsedAnalysisResults.get(i);
	}

	@Override
	public String toString() {
		return "ParsedAnalysisResultFile [sessionName=" + sessionName + ", Number of Wells= " + parsedAnalysisResults.size() +  "]";
	}
}
