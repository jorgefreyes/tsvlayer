package com.fulgent.tsvlayer;

import java.io.IOException;

public class MainDriver {
	
	public static void main(String[] args) {
		final String in = args[0];
		ParsedAnalysisResultFile outFile = null;
		
		ReadAnalysisResultFile f = new ReadAnalysisResultFile(in);
				
		try {
			outFile = f.parseAnalysisResultFile(in);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(outFile.toString());
		
		System.exit(0);
	}
	

}
